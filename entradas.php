<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="estilos.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="completar.css">
  <script type="text/javascript" src="completar.js"></script>
  <script type="text/javascript">
    function MM_validateForm() { //v4.0
      if (document.getElementById) {
        var i, p, q, nm, test, num, min, max, errors = '',
          args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
          test = args[i + 2];
          val = document.getElementById(args[i]);
          if (val) {
            nm = val.name;
            if ((val = val.value) != "") {
              if (test.indexOf('isEmail') != -1) {
                p = val.indexOf('@');
                if (p < 1 || p == (val.length - 1)) errors += '- ' + nm + ' must contain an e-mail address.\n';
              } else if (test != 'R') {
                num = parseFloat(val);
                if (isNaN(val)) errors += '- ' + nm + ' must contain a number.\n';
                if (test.indexOf('inRange') != -1) {
                  p = test.indexOf(':');
                  min = test.substring(8, p);
                  max = test.substring(p + 1);
                  if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                }
              }
            } else if (test.charAt(0) == 'R') errors += '- ' + nm + ' es obligatorio.\n';
          }
        }
        if (errors) alert('Tienes un error:\n' + errors);
        document.MM_returnValue = (errors == '');
      }
    }
  </script>
</head>
<script type="text/javascript">
    function trat_previo(sel) {
        if (sel.value == "SI") {
            divC = document.getElementById("IMEI_SI");
            divC.style.display = "";
        }
        if (sel.value != "SI") {
            divC = document.getElementById("IMEI_SI");
            divC.style.display = "none";
        }
    }
    </script>
<body onload="asignaVariables();">
  <?PHP
  //require('include_sup.php'); //trae la pagina//
  $string_intro = getenv("QUERY_STRING");
  parse_str($string_intro);
  if ($t_coor == 1) {
    include('include_sup.php');
  }
  if ($t_coor == 2) {
    include('include_sup2-t3.php');
  }
  if ($t_coor == 3) {
    include('include_sup-t3.php');
  }
  if ($t_coor == 5) {
    include('include_sup-t5.php');
  }
  $string_intro = getenv("QUERY_STRING");
  parse_str($string_intro);
  require('conex.php');
  $Conexion = mysqli_connect($servidor, $usuario, $password) or die("No se Puede conectar al Servidor");
  mysqli_select_db($Conexion, $baseinvent) or die("No se Puede conectar a la base de Datos");
  ?>
  <div align="center">
    <table width="900" border="0" align="center">
      <tr>
        <th width="987" height="14" class="titulo_formulario" scope="col">INGRESO DE ARTICULO <br> </th>
      </tr>
      <tr>
        <td><br>
          <!--  <form action="subirarchivocalidad.php" target="inserform" method="post" enctype="multipart/form-data"> -->
          <form action="ingreso_dispositivo.php?coor_id=<?php echo $coor_id; ?>&t_coor=<?php echo $t_coor; ?>&evento=<?php echo $evento; ?>" method="post" enctype="multipart/form-data" name="InsertarCliente" id="InsertarCliente" onSubmit="MM_validateForm('input_2','','R','UNIDADES','','RisNum');return document.MM_returnValue">
            <table width="439" border="1" align="center" cellpadding="5" cellspacing="5" class="titulo">
              <tr>
                <th width="199" class="texto_formulario" scope="col">
                  <div align="left">
                    <label for="checkbox_row_2">NOMBRE PROVEEDOR</label>
                    :
                  </div>
                </th>
                <th width="199" scope="col">
                  <div align="left">
                    <input name="NOM_PROVEE" type="text" class="campos_formulario" id="NOM_PROVEE" onClick="value=''" size="30">
                  </div>
                </th>
              </tr>
              <tr>
                <th class="texto_formulario">
                  <div align="left">NO remision PROVEEDOR:</div>
                </th>
                <td>
                  <div align="left">
                    <input name="NO_REMICION" type="text" class="campos_formulario" id="NO_REMICION" onClick="value=''" size="30">
                  </div>
                </td>
              </tr>
              <tr>
                <th class="texto_formulario" scope="col">
                  <div align="left">
                    <label for="checkbox_row_2">REFERENCIA</label>
                  </div>
                </th>
                <th scope="col">
                  <div align="left">
                    <input name="REFERENCIA" type="text" class="campos_formulario" id="REFERENCIA" placeholder="referencia" onClick="value=''" size="30">
                  </div>
                </th>
              </tr>
              <tr>
                <th class="texto_formulario" scope="col">
                  <div align="left">
                    <label for="checkbox_row_2">MARCA</label>
                  </div>
                </th>
                <th scope="col">
                  <div align="left">
                    <input name="MARCA" type="text" class="campos_formulario" id="MARCA" placeholder="Marca" size="30">
                  </div>
                </th>
              </tr>
              <tr>
                <th class="texto_formulario" scope="col">
                  <div align="left">
                    <label for="checkbox_row_2">REQUIERE IMEI <span style="font-size: 15px;">*</span></label>
                  </div>
                </th>
                <th scope="col">
                  <div align="left">
                    <select name="IMEI_R" id="IMEI_R" style="width: 53%;" onchange="trat_previo(this)">
                      <option>-Requiere imei-</option>
                      <option value="SI">SI</option>
                      <option value="NO">NO</option>
                    </select>
                    <div id="IMEI_SI" style="display:none">
                                    <span>Imei</span>
                                    <input name="IMEI" id="IMEI" type="text"
                                        style="width:78%;" />
                                </div>
                  </div>
                </th>
              </tr>
              <tr>
                <th class="texto_formulario">
                  <div align="left">NOMBRE DEL ARTICULO: <span style="font-size: 15px;">*</span></div>
                </th>
                <td>
                  <div id="demoDer" onfocus="MM_validateForm('input_2','','R');return document.MM_returnValue">
                    <input name="NOMBRE_DEL_ARTICULO" autocomplete="off" type="text" class="campos_formulario" id="input_2" onfocus="if(document.getElementById('lista').childNodes[0]!=null && this.value!='') { filtraLista(this.value); formateaLista(this.value); 
                    reiniciaSeleccion(); document.getElementById('lista').style.display='block'; }" onblur="if(v==1) document.getElementById('lista').style.display='none';MM_validateForm('input_2','','R','UNIDADES','','R');return document.MM_returnValue" onkeyup="if(navegaTeclado(event)==1) {
                    clearTimeout(ultimoIdentificador); 
                    ultimoIdentificador=setTimeout('rellenaLista()', 1000); }" size="38">
                    <div id="lista" onmouseout="v=1;" onmouseover="v=0;">
                    </div>
                  </div>
                  <div class="mensaje" id="error"></div>
                </td>
              </tr>
              <!----------------------------------------------------------------------------------------------------------------------------------->
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">VALOR UNI: <span style="font-size: 15px;">*</span></div>
                </th>
                <td>
                  <div align="left">
                    <input name="VALOR_UNI" type="text" class="campos_formulario" id="VALOR_UNI" size="30">
                  </div>
                </td>
              </tr>
              <!----------------------------------------------------------------------------------------------------------------------------------->
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">ID PRODUCTO LIVETRADE: <span style="font-size: 15px;">*</span></div>
                </th>
                <td>
                  <div align="left">
                    <input name="ID_PRODUCT" type="text" class="campos_formulario" id="ID_PRODUCT" size="30">
                  </div>
                </td>
              </tr>
              <!----------------------------------------------------------------------------------------------------------------------------------->
              <tr>
                <th class="texto_formulario">
                  <div align="left">DESCRIPCI&oacute;N DEL DISPOSITIVO:</div>
                </th>
                <td align="left" valign="middle">
                  <div align="left">
                    <textarea name="DESCRIPCION_DEL_DISPOSITIVO" cols="30" rows="5" class="campos_formulario" id="DESCRIPCION_DEL_DISPOSITIVO" placeholder="Breve descripcion"></textarea>
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">ESTADO DEL PRODUCTO:</div>
                </th>
                <td>
                  <div align="left">
                    <select name="ESTADO_PRODUCTO" id="ESTADO_PRODUCTO">
                      <option>--Seleccione El Estado--</option>
                      <option>Activo</option>
                      <option>Inactivo</option>
                      <option>Reservado</option>
                      <option>Agotado</option>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <th class="texto_formulario" scope="col">
                  <div align="left">
                    <label for="checkbox_row_2">FECHA DE CADUCACION:</label>
                  </div>
                </th>
                <th scope="col">
                  <div align="left">
                    <input name="FECHA_VENC" type="date" class="campos_formulario" id="FECHA_VENC" size="30">
                  </div>
                </th>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">CLIENTE:</div>
                </th>
                <td>
                  <div align="left">
                    <select name="CLIENTE" id="CLIENTE">
                      <option>--Seleccione El Cliente--</option>
                      <?PHP
                      $Seleccion = mysqli_query($Conexion, "SELECT * FROM cliente GROUP BY nombre ORDER BY nombre");
                      while ($fila = mysqli_fetch_array($Seleccion)) {
                        echo "<option>" . $fila['nombre'] . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">UBICACI&Oacute;N:</div>
                </th>
                <td>
                  <div align="left">
                    <input name="PRODUCTO" id="PRODUCTO" value="">
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">ACTIVIDAD:</div>
                </th>
                <td>
                  <div align="left">
                    <select name="ACTIVIDAD" id="ACTIVIDAD">
                      <option>--Seleccione La Actividad--</option>
                      <?PHP
                      $Seleccion = mysqli_query($Conexion, "SELECT * FROM cliente GROUP BY actividad ORDER BY actividad");
                      while ($fila = mysqli_fetch_array($Seleccion)) {
                        echo "<option>" . $fila['actividad'] . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">FOTO: <span style="font-size: 15px;">*</span></div>
                </th>
                <td>
                  <div align="left">
                    <!-- <p><b class="Estilo2">Nombre Archivo:</b> <br>
                  <input type="text" name="cadenatexto2" size="20" maxlength="100"> -->
                    <input type="hidden" name="MAX_FILE_SIZE" value="100000000">
                    <input name="userfile" type="file" size="15" />
                    <!--  <input name="calidad" type="submit" id="calidad" value="Enviar"> -->
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">PUNTO DE VENTA:</div>
                </th>
                <td>
                  <div align="left">
                    <!-- <input name="BODEGA" type="text" class="campos_formulario" id="BODEGA"value="Bodega"  onClick="value=''" size="30"> -->
                    <select name="BODEGA" id="BODEGA">
                      <option>--Seleccione el punto--</option>
                      <?PHP
                      $Seleccion = mysqli_query($Conexion, "SELECT * FROM bodegas ORDER BY nombre_bod");
                      while ($fila = mysqli_fetch_array($Seleccion)) {
                        echo "<option>" . $fila['nombre_bod'] . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">UNIDADES: <span style="font-size: 15px;">*</span></div>
                </th>
                <td>
                  <div align="left">
                    <input name="UNIDADES" type="text" class="campos_formulario" id="UNIDADES" onClick="value=''" size="30">
                  </div>
                </td>
              </tr>
              <tr>
                <th height="32" class="texto_formulario">
                  <div align="left">OBSEVACION:</div>
                </th>
                <td>
                  <div align="left">
                    <textarea name="OBSERVACION" cols="30" class="campos_formulario" id="OBSERVACION" onClick="value=''"></textarea>
                  </div>
                </td>
              </tr>
              <tr>
                <TD height="35" colspan="2" align="center" valign="middle" class="texto_formulario">
                  <div align="center">
                    <input name="Agregar" type="submit" class="botones" id="Agregar" onClick="MM_validateForm('input_2','','R','UNIDADES','','R');return document.MM_returnValue" value="Agregar">
                    &nbsp;
                    <input name="reset" type="reset" class="botones" id="reset" value="Limpiar">
                  </div>
                </TD>
              </tr>
            </table>
          </form>
        </td>
      </tr>
      <tr>
        <td><br>
          <br> <?php //include('include_inf.php'); 
                ?>
        </td> <!-- trae la pagina y la incluye en el boton -->
      </tr>
    </table>
  </div>
</body>
</html>