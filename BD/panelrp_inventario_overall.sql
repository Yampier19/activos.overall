-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-02-2022 a las 22:21:13
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `panelrp_inventario_overall`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activos_map`
--

CREATE TABLE `activos_map` (
  `id` bigint(10) NOT NULL,
  `id_maps` bigint(10) DEFAULT NULL,
  `fecha` varchar(20) DEFAULT NULL,
  `responsable` varchar(50) DEFAULT NULL,
  `bodega` varchar(40) DEFAULT NULL,
  `cliente` varchar(30) DEFAULT NULL,
  `articulo` varchar(200) DEFAULT NULL,
  `codigo` bigint(10) DEFAULT NULL,
  `referencia` varchar(20) DEFAULT NULL,
  `stock` int(10) DEFAULT NULL,
  `canti_salida` int(10) DEFAULT NULL,
  `lugar_entrega` varchar(200) DEFAULT NULL,
  `devolucion` int(1) DEFAULT NULL,
  `fecha_devolucion` varchar(20) DEFAULT NULL,
  `destinatario` varchar(100) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `nombre_contacto` varchar(50) DEFAULT NULL,
  `tel_contacto` varchar(20) DEFAULT NULL,
  `transportador` varchar(50) DEFAULT NULL,
  `observaciones` varchar(400) DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodegas`
--

CREATE TABLE `bodegas` (
  `id` bigint(5) NOT NULL,
  `nombre_bod` varchar(50) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `telefono` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `bodegero` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bodegas`
--

INSERT INTO `bodegas` (`id`, `nombre_bod`, `direccion`, `telefono`, `bodegero`) VALUES
(1, 'P_Venta 1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` bigint(10) NOT NULL,
  `nombre` varchar(50) CHARACTER SET latin1 NOT NULL,
  `producto` varchar(70) CHARACTER SET latin1 DEFAULT NULL,
  `actividad` varchar(70) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `producto`, `actividad`) VALUES
(1, 'Honor Honduras', 'Celular Honor 8 X', 'Venta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE `entradas` (
  `id` bigint(10) NOT NULL,
  `fecha` varchar(25) CHARACTER SET latin1 NOT NULL,
  `codigo` bigint(20) NOT NULL,
  `referencia` varchar(20) CHARACTER SET latin1 NOT NULL,
  `producto` varchar(200) CHARACTER SET latin1 NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `proveedor` varchar(100) CHARACTER SET latin1 NOT NULL,
  `rem_proveedor` varchar(20) CHARACTER SET latin1 NOT NULL,
  `bodega` varchar(30) CHARACTER SET latin1 NOT NULL,
  `usuario` varchar(30) CHARACTER SET latin1 NOT NULL,
  `observaciones` text NOT NULL,
  `cliente` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `id_referencia` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `fecha`, `codigo`, `referencia`, `producto`, `cantidad`, `proveedor`, `rem_proveedor`, `bodega`, `usuario`, `observaciones`, `cliente`, `id_referencia`) VALUES
(1, '2022-01-11 11:08:43', 0, 'honor', 'Celular Honor 8X', 20, 'honor', 'honor', '--Seleccione el punto--', 'Sistemas', 'Prueba', 'Honor Honduras', 0),
(2, '2022-01-11 11:11:13', 1, 'honor', 'Honor', 20, 'honor', 'honor', 'P_Venta 1', 'Sistemas', 'Honor', 'Honor Honduras', 0),
(3, '2022-01-11 11:22:10', 1, 'Honor', 'Honor 10 lite', 200, 'Honor', 'Honor', 'P_Venta 1', 'Sistemas', 'Honor', 'Honor Honduras', 0),
(4, '2022-01-11 13:41:56', 4, 'Honor', 'Celular honor 10', 700, 'Honor', 'Honor', 'P_Venta 1', 'Sistemas', 'Celular honor 10', 'Honor Honduras', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas_11-07-2019`
--

CREATE TABLE `entradas_11-07-2019` (
  `id` bigint(10) NOT NULL,
  `fecha` varchar(20) CHARACTER SET latin1 NOT NULL,
  `codigo` bigint(20) NOT NULL,
  `referencia` varchar(20) CHARACTER SET latin1 NOT NULL,
  `producto` varchar(200) CHARACTER SET latin1 NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `proveedor` varchar(100) CHARACTER SET latin1 NOT NULL,
  `rem_proveedor` varchar(20) CHARACTER SET latin1 NOT NULL,
  `bodega` varchar(30) CHARACTER SET latin1 NOT NULL,
  `usuario` varchar(30) CHARACTER SET latin1 NOT NULL,
  `observaciones` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cliente` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `id_referencia` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas_2018-09-20`
--

CREATE TABLE `entradas_2018-09-20` (
  `id` bigint(10) NOT NULL,
  `fecha` varchar(20) CHARACTER SET latin1 NOT NULL,
  `codigo` bigint(20) NOT NULL,
  `referencia` varchar(20) CHARACTER SET latin1 NOT NULL,
  `producto` varchar(200) CHARACTER SET latin1 NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `proveedor` varchar(100) CHARACTER SET latin1 NOT NULL,
  `rem_proveedor` varchar(20) CHARACTER SET latin1 NOT NULL,
  `bodega` varchar(30) CHARACTER SET latin1 NOT NULL,
  `usuario` varchar(30) CHARACTER SET latin1 NOT NULL,
  `observaciones` varchar(100) CHARACTER SET latin1 NOT NULL,
  `cliente` varchar(20) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas_temp`
--

CREATE TABLE `entradas_temp` (
  `id` int(11) NOT NULL,
  `codigo` varchar(300) DEFAULT NULL,
  `bodega` varchar(300) DEFAULT NULL,
  `nombre_del_dispositivo` varchar(300) DEFAULT NULL,
  `unidades` int(11) DEFAULT NULL,
  `nom_provee` varchar(300) DEFAULT NULL,
  `nom_remision` varchar(300) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_usuario` varchar(300) DEFAULT NULL,
  `id_unico` varchar(300) DEFAULT NULL,
  `ubicacion` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE `historial` (
  `ID` bigint(10) NOT NULL,
  `TIPO_MOVIMIENTO` int(5) NOT NULL,
  `CLIENTE` varchar(20) CHARACTER SET latin1 NOT NULL,
  `CODIGO_PRODUCTO` bigint(10) NOT NULL,
  `NOMBRE_PRODUCTO` varchar(100) CHARACTER SET latin1 NOT NULL,
  `CANTIDAD` int(5) NOT NULL,
  `PROVEEDOR` varchar(50) CHARACTER SET latin1 NOT NULL,
  `NO_REMICION_PROVEE` varchar(20) CHARACTER SET latin1 NOT NULL,
  `BODEGA_SALIDA` varchar(20) CHARACTER SET latin1 NOT NULL,
  `BODEGA_ENTRADA` varchar(20) CHARACTER SET latin1 NOT NULL,
  `RESPONSABLE` varchar(20) CHARACTER SET latin1 NOT NULL,
  `FECHA_MOVIMIENTO` varchar(20) CHARACTER SET latin1 NOT NULL,
  `FECHA_DEVOLUCION` varchar(20) CHARACTER SET latin1 NOT NULL,
  `LUGAR` varchar(100) CHARACTER SET latin1 NOT NULL,
  `EN_ACTIVIDAD` int(5) NOT NULL,
  `DESTINATARIO` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `DIRECCION` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `NOMBRE_CONTACTO` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `TELEFONO` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `ESTADO_REMI_PEOPLE` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `TRANSPORTADOR` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `OBSERVACIONES` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `NO_REMICION_PEOPLE` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `ESTADO` varchar(20) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`ID`, `TIPO_MOVIMIENTO`, `CLIENTE`, `CODIGO_PRODUCTO`, `NOMBRE_PRODUCTO`, `CANTIDAD`, `PROVEEDOR`, `NO_REMICION_PROVEE`, `BODEGA_SALIDA`, `BODEGA_ENTRADA`, `RESPONSABLE`, `FECHA_MOVIMIENTO`, `FECHA_DEVOLUCION`, `LUGAR`, `EN_ACTIVIDAD`, `DESTINATARIO`, `DIRECCION`, `NOMBRE_CONTACTO`, `TELEFONO`, `ESTADO_REMI_PEOPLE`, `TRANSPORTADOR`, `OBSERVACIONES`, `NO_REMICION_PEOPLE`, `ESTADO`) VALUES
(1, 1, 'Honor Honduras', 0, 'Celular Honor 8X', 20, 'honor', 'honor', 'N/A', '--Seleccione el punt', '', '2022-01-11 11:08:43', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'Honor Honduras', 1, 'Honor', 20, 'honor', 'honor', 'N/A', 'P_Venta 1', '', '2022-01-11 11:11:13', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 'Honor Honduras', 1, 'Honor 10 lite', 200, 'Honor', 'Honor', 'N/A', 'P_Venta 1', '', '2022-01-11 11:22:10', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 'Honor Honduras', 4, 'Celular honor 10', 700, 'Honor', 'Honor', 'N/A', 'P_Venta 1', '', '2022-01-11 13:41:56', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial-2018-09-20`
--

CREATE TABLE `historial-2018-09-20` (
  `ID` bigint(10) NOT NULL,
  `TIPO_MOVIMIENTO` int(5) NOT NULL,
  `CLIENTE` varchar(20) NOT NULL,
  `CODIGO_PRODUCTO` bigint(10) NOT NULL,
  `NOMBRE_PRODUCTO` varchar(100) NOT NULL,
  `CANTIDAD` int(5) NOT NULL,
  `PROVEEDOR` varchar(50) NOT NULL,
  `NO_REMICION_PROVEE` varchar(20) NOT NULL,
  `BODEGA_SALIDA` varchar(20) NOT NULL,
  `BODEGA_ENTRADA` varchar(20) NOT NULL,
  `RESPONSABLE` varchar(20) NOT NULL,
  `FECHA_MOVIMIENTO` varchar(20) NOT NULL,
  `FECHA_DEVOLUCION` varchar(20) NOT NULL,
  `LUGAR` varchar(100) NOT NULL,
  `EN_ACTIVIDAD` int(5) NOT NULL,
  `DESTINATARIO` varchar(50) DEFAULT NULL,
  `DIRECCION` varchar(100) DEFAULT NULL,
  `NOMBRE_CONTACTO` varchar(50) DEFAULT NULL,
  `TELEFONO` varchar(30) DEFAULT NULL,
  `ESTADO_REMI_PEOPLE` varchar(20) DEFAULT NULL,
  `TRANSPORTADOR` varchar(50) DEFAULT NULL,
  `OBSERVACIONES` varchar(200) DEFAULT NULL,
  `NO_REMICION_PEOPLE` varchar(20) DEFAULT NULL,
  `ESTADO` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_backup`
--

CREATE TABLE `historial_backup` (
  `ID` bigint(10) NOT NULL,
  `TIPO_MOVIMIENTO` int(5) NOT NULL,
  `CLIENTE` varchar(20) NOT NULL,
  `CODIGO_PRODUCTO` bigint(10) NOT NULL,
  `NOMBRE_PRODUCTO` varchar(100) NOT NULL,
  `CANTIDAD` int(5) NOT NULL,
  `PROVEEDOR` varchar(50) NOT NULL,
  `NO_REMICION_PROVEE` varchar(20) NOT NULL,
  `BODEGA_SALIDA` varchar(20) NOT NULL,
  `BODEGA_ENTRADA` varchar(20) NOT NULL,
  `RESPONSABLE` varchar(20) NOT NULL,
  `FECHA_MOVIMIENTO` varchar(20) NOT NULL,
  `FECHA_DEVOLUCION` varchar(20) NOT NULL,
  `LUGAR` varchar(100) NOT NULL,
  `EN_ACTIVIDAD` int(5) NOT NULL,
  `DESTINATARIO` varchar(50) DEFAULT NULL,
  `DIRECCION` varchar(100) DEFAULT NULL,
  `NOMBRE_CONTACTO` varchar(50) DEFAULT NULL,
  `TELEFONO` varchar(30) DEFAULT NULL,
  `ESTADO_REMI_PEOPLE` varchar(20) DEFAULT NULL,
  `TRANSPORTADOR` varchar(50) DEFAULT NULL,
  `OBSERVACIONES` varchar(200) DEFAULT NULL,
  `NO_REMICION_PEOPLE` varchar(20) DEFAULT NULL,
  `ESTADO` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_cocacola`
--

CREATE TABLE `instal_cocacola` (
  `NO_REF` int(6) NOT NULL,
  `PLAN` varchar(14) DEFAULT NULL,
  `CODIGO_CLIENTE` bigint(15) DEFAULT NULL,
  `GERENCIA` varchar(4) DEFAULT NULL,
  `ZONA` varchar(3) DEFAULT NULL,
  `RUTA` varchar(6) DEFAULT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `NEGOCIO` varchar(70) DEFAULT NULL,
  `BARRIO` varchar(50) DEFAULT NULL,
  `DIRECCION` varchar(50) DEFAULT NULL,
  `TELEFONO` varchar(21) DEFAULT NULL,
  `FISICO` varchar(3) DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `ESTADO` int(2) DEFAULT 0,
  `USUARIO` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_cocacola1`
--

CREATE TABLE `instal_cocacola1` (
  `NO_REF` int(6) NOT NULL,
  `PLAN` varchar(14) DEFAULT NULL,
  `CODIGO_CLIENTE` bigint(15) DEFAULT NULL,
  `GERENCIA` varchar(4) DEFAULT NULL,
  `ZONA` varchar(3) DEFAULT NULL,
  `RUTA` varchar(6) DEFAULT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `NEGOCIO` varchar(70) DEFAULT NULL,
  `BARRIO` varchar(50) DEFAULT NULL,
  `DIRECCION` varchar(50) DEFAULT NULL,
  `TELEFONO` varchar(21) DEFAULT NULL,
  `FISICO` varchar(3) DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `ESTADO` int(2) DEFAULT 0,
  `USUARIO` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_cocacola2`
--

CREATE TABLE `instal_cocacola2` (
  `NO_REF` int(6) NOT NULL,
  `PLAN` varchar(14) DEFAULT NULL,
  `CODIGO_CLIENTE` bigint(15) DEFAULT NULL,
  `GERENCIA` varchar(4) DEFAULT NULL,
  `ZONA` varchar(3) DEFAULT NULL,
  `RUTA` varchar(6) DEFAULT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `NEGOCIO` varchar(70) DEFAULT NULL,
  `BARRIO` varchar(50) DEFAULT NULL,
  `DIRECCION` varchar(50) DEFAULT NULL,
  `TELEFONO` varchar(21) DEFAULT NULL,
  `FISICO` varchar(3) DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `ESTADO` int(2) DEFAULT 0,
  `USUARIO` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_cocacola_ant`
--

CREATE TABLE `instal_cocacola_ant` (
  `ID` int(2) DEFAULT NULL,
  `CADENA` varchar(20) DEFAULT NULL,
  `CODIGO` int(10) DEFAULT NULL,
  `PUNTO_VENTA` varchar(35) DEFAULT NULL,
  `CIUDAD` varchar(15) DEFAULT NULL,
  `USUARIO` varchar(10) DEFAULT NULL,
  `PUNTA_GONDOLA` bigint(10) DEFAULT NULL,
  `MAMUT` bigint(10) DEFAULT NULL,
  `MUEBLEBRISA` bigint(10) DEFAULT NULL,
  `NOMBRE_ARTICULO` varchar(30) DEFAULT NULL,
  `CANTIDAD` varchar(10) DEFAULT NULL,
  `FECHA_INSTAL` varchar(10) DEFAULT NULL,
  `FECHA_CREACION` varchar(10) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `ESTADO` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_cocacola_ante`
--

CREATE TABLE `instal_cocacola_ante` (
  `NO_REF` int(6) NOT NULL,
  `PLAN` varchar(14) DEFAULT NULL,
  `CODIGO_CLIENTE` bigint(15) DEFAULT NULL,
  `GERENCIA` varchar(4) DEFAULT NULL,
  `ZONA` varchar(3) DEFAULT NULL,
  `RUTA` varchar(6) DEFAULT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `NEGOCIO` varchar(70) DEFAULT NULL,
  `BARRIO` varchar(50) DEFAULT NULL,
  `DIRECCION` varchar(50) DEFAULT NULL,
  `TELEFONO` varchar(21) DEFAULT NULL,
  `FISICO` varchar(3) DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `ESTADO` int(2) DEFAULT 0,
  `USUARIO` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_distribuidor`
--

CREATE TABLE `instal_distribuidor` (
  `ID` int(3) DEFAULT NULL,
  `CANAL` varchar(14) DEFAULT NULL,
  `PUNTO_N` int(6) DEFAULT NULL,
  `DISTRIBUIDOR` varchar(52) DEFAULT NULL,
  `DIRECCION` varchar(67) DEFAULT NULL,
  `DEPARTAMENTO` varchar(10) DEFAULT NULL,
  `CIUDAD` varchar(21) DEFAULT NULL,
  `REGION` varchar(8) DEFAULT NULL,
  `CELULAR` varchar(17) DEFAULT NULL,
  `ESTADO` int(1) DEFAULT NULL,
  `STICKER_FACHADA` int(1) DEFAULT NULL,
  `STICKER_BABERO` int(5) DEFAULT NULL,
  `BOTONES` int(5) DEFAULT NULL,
  `TROPEZON` int(5) DEFAULT NULL,
  `AFICHES` int(5) DEFAULT NULL,
  `MOVIL_3D` int(5) DEFAULT NULL,
  `MOVIL_BANDERIN` int(5) DEFAULT NULL,
  `VOLANTES` int(5) DEFAULT NULL,
  `CHISPAS` int(5) DEFAULT NULL,
  `SALTARINES` int(5) DEFAULT NULL,
  `HABLADOR` int(5) DEFAULT NULL,
  `LAMINAS_TRIPTICO` int(5) DEFAULT NULL,
  `ALETA_COMPUTADOR` int(5) DEFAULT NULL,
  `ACRILICO_BARRA` int(5) DEFAULT NULL,
  `AFICHE_POLIESTILENO` int(5) DEFAULT NULL,
  `ALETAS_CADENAS` varchar(10) DEFAULT NULL,
  `CABEZOTES` varchar(10) DEFAULT NULL,
  `MOVIL_PLANO` int(5) DEFAULT NULL,
  `AI_STICKER_FACHADA` int(5) DEFAULT NULL,
  `AI_STICKER_BABERO` int(5) DEFAULT NULL,
  `AI_BOTONES` int(5) DEFAULT NULL,
  `AI_TROPEZON_CLARO` int(5) DEFAULT NULL,
  `AI_AFICHES` int(5) DEFAULT NULL,
  `AI_MOVIL_3D` int(5) DEFAULT NULL,
  `AI_MOVIL_BANDERIN` int(5) DEFAULT NULL,
  `AI_VOLANTES` int(5) DEFAULT NULL,
  `AI_CHISPAS` int(5) DEFAULT NULL,
  `AI_SALTARINES` int(5) DEFAULT NULL,
  `AI_HABLADOR` int(5) DEFAULT NULL,
  `AI_LAMINAS_TRIPTICO` int(5) DEFAULT NULL,
  `AI_ALETA_COMPUTADOR` int(5) DEFAULT NULL,
  `AI_ACRILICO_BARRA` int(5) DEFAULT NULL,
  `AI_VOLANTES_CLARO` int(5) DEFAULT NULL,
  `AI_HABLADOR_CLARO` int(5) DEFAULT NULL,
  `AI_SALTARIN_POSTPAGO_CLARO` int(5) DEFAULT NULL,
  `AI_TRIPTICO_CLARO` int(5) DEFAULT NULL,
  `AI_AFICHES_CLARO` int(5) DEFAULT NULL,
  `AI_TROPEZON_POSTPAGO_CLARO` int(5) DEFAULT NULL,
  `TRIPTICO_CLARO` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_gestion`
--

CREATE TABLE `instal_gestion` (
  `ID` bigint(5) NOT NULL,
  `ID_DISTRIBUIDOR` bigint(5) DEFAULT NULL,
  `NO_VISITA` bigint(3) DEFAULT NULL,
  `STICKER_FACHADA` bigint(3) DEFAULT NULL,
  `STICKER_BABERO` bigint(3) DEFAULT NULL,
  `BOTONES` bigint(3) DEFAULT NULL,
  `TROPEZON` bigint(3) DEFAULT NULL,
  `AFICHES` bigint(3) DEFAULT NULL,
  `MOVIL_3D` bigint(3) DEFAULT NULL,
  `MOVIL_BANDERIN` bigint(3) DEFAULT NULL,
  `VOLANTES` bigint(3) DEFAULT NULL,
  `CHISPAS` bigint(3) DEFAULT NULL,
  `SALTARINES` bigint(3) DEFAULT NULL,
  `HABLADOR` bigint(3) DEFAULT NULL,
  `LAMINAS_TRIPTICO` bigint(3) DEFAULT NULL,
  `ALETA_COMPUTADOR` bigint(3) DEFAULT NULL,
  `ACRILICO_BARRA` bigint(3) DEFAULT NULL,
  `AFICHE_POLIESTILENO` bigint(20) DEFAULT NULL,
  `ALETAS_CADENAS` bigint(3) DEFAULT NULL,
  `CABEZOTES` bigint(3) DEFAULT NULL,
  `MOVIL_PLANO` bigint(3) DEFAULT NULL,
  `FECHA_CREACION` varchar(20) DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_gestion_cocacola`
--

CREATE TABLE `instal_gestion_cocacola` (
  `ID` bigint(5) NOT NULL,
  `ID_DISTRIBUIDOR` bigint(5) DEFAULT NULL,
  `NO_VISITA` bigint(3) DEFAULT NULL,
  `CABEZOTES` bigint(3) DEFAULT NULL,
  `FECHA_CREACION` varchar(20) DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `USUARIO` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_maggi`
--

CREATE TABLE `instal_maggi` (
  `FECHA_CARAVANA` varchar(5) DEFAULT NULL,
  `NO` int(3) DEFAULT NULL,
  `CARAVANA` varchar(9) DEFAULT NULL,
  `CANAL` varchar(15) DEFAULT NULL,
  `NOMBRE_PDV` varchar(35) DEFAULT NULL,
  `DIRECCION` varchar(25) DEFAULT NULL,
  `BARRIO` varchar(30) DEFAULT NULL,
  `CIUDAD` varchar(13) DEFAULT NULL,
  `TENDERO_ADMINISTRADOR` varchar(31) DEFAULT NULL,
  `TELEFONO_PDV` varchar(10) DEFAULT NULL,
  `VENDEDOR` varchar(16) DEFAULT NULL,
  `TELEFONO_VENDEDOR` bigint(11) DEFAULT NULL,
  `SET_DE_CUBIERTOS` varchar(10) DEFAULT NULL,
  `PRACTITARRO` varchar(10) DEFAULT NULL,
  `RAYADOR` varchar(10) DEFAULT NULL,
  `BOWL` varchar(10) DEFAULT NULL,
  `SET_JARRA_CON_VASO` varchar(10) DEFAULT NULL,
  `OLLA_ARROCERA` varchar(10) DEFAULT NULL,
  `TARRO_PRIMAVERA` varchar(10) DEFAULT NULL,
  `JARRA_1LT` varchar(10) DEFAULT NULL,
  `JARRA_2Lt` varchar(10) DEFAULT NULL,
  `ESCURRIDOR_DE_PLATOS` varchar(10) DEFAULT NULL,
  `CALCULADORA` varchar(10) DEFAULT NULL,
  `RELOJ` varchar(10) DEFAULT NULL,
  `BUTACO` varchar(10) DEFAULT NULL,
  `ESCALERILLA` varchar(10) DEFAULT NULL,
  `VAJILLA` varchar(10) DEFAULT NULL,
  `SET_DE_OLLAS` varchar(10) DEFAULT NULL,
  `TABLET` varchar(10) DEFAULT NULL,
  `BICICLETA` varchar(10) DEFAULT NULL,
  `COLORES` varchar(10) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `GESTIONADO` int(1) DEFAULT 0,
  `FECHA_GESTION` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_maggi_07-04-2016`
--

CREATE TABLE `instal_maggi_07-04-2016` (
  `FECHA_CARAVANA` varchar(5) DEFAULT NULL,
  `NO` int(3) DEFAULT NULL,
  `CARAVANA` varchar(9) DEFAULT NULL,
  `CANAL` varchar(15) DEFAULT NULL,
  `NOMBRE_PDV` varchar(35) DEFAULT NULL,
  `DIRECCION` varchar(25) DEFAULT NULL,
  `BARRIO` varchar(30) DEFAULT NULL,
  `CIUDAD` varchar(13) DEFAULT NULL,
  `TENDERO_ADMINISTRADOR` varchar(31) DEFAULT NULL,
  `TELEFONO_PDV` varchar(10) DEFAULT NULL,
  `VENDEDOR` varchar(16) DEFAULT NULL,
  `TELEFONO_VENDEDOR` bigint(11) DEFAULT NULL,
  `SET_DE_CUBIERTOS` varchar(10) DEFAULT NULL,
  `PRACTITARRO` varchar(10) DEFAULT NULL,
  `RAYADOR` varchar(10) DEFAULT NULL,
  `BOWL` varchar(10) DEFAULT NULL,
  `SET_JARRA_CON_VASO` varchar(10) DEFAULT NULL,
  `OLLA_ARROCERA` varchar(10) DEFAULT NULL,
  `TARRO_PRIMAVERA` varchar(10) DEFAULT NULL,
  `JARRA_1LT` varchar(10) DEFAULT NULL,
  `JARRA_2Lt` varchar(10) DEFAULT NULL,
  `ESCURRIDOR_DE_PLATOS` varchar(10) DEFAULT NULL,
  `CALCULADORA` varchar(10) DEFAULT NULL,
  `RELOJ` varchar(10) DEFAULT NULL,
  `BUTACO` varchar(10) DEFAULT NULL,
  `ESCALERILLA` varchar(10) DEFAULT NULL,
  `VAJILLA` varchar(10) DEFAULT NULL,
  `SET_DE_OLLAS` varchar(10) DEFAULT NULL,
  `TABLET` varchar(10) DEFAULT NULL,
  `BICICLETA` varchar(10) DEFAULT NULL,
  `COLORES` varchar(10) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `GESTIONADO` int(1) DEFAULT 0,
  `FECHA_GESTION` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instal_maggi_13-04-2016`
--

CREATE TABLE `instal_maggi_13-04-2016` (
  `FECHA_CARAVANA` varchar(5) DEFAULT NULL,
  `NO` int(3) DEFAULT NULL,
  `CARAVANA` varchar(9) DEFAULT NULL,
  `CANAL` varchar(15) DEFAULT NULL,
  `NOMBRE_PDV` varchar(35) DEFAULT NULL,
  `DIRECCION` varchar(25) DEFAULT NULL,
  `BARRIO` varchar(30) DEFAULT NULL,
  `CIUDAD` varchar(13) DEFAULT NULL,
  `TENDERO_ADMINISTRADOR` varchar(31) DEFAULT NULL,
  `TELEFONO_PDV` varchar(10) DEFAULT NULL,
  `VENDEDOR` varchar(16) DEFAULT NULL,
  `TELEFONO_VENDEDOR` bigint(11) DEFAULT NULL,
  `SET_DE_CUBIERTOS` varchar(10) DEFAULT NULL,
  `PRACTITARRO` varchar(10) DEFAULT NULL,
  `RAYADOR` varchar(10) DEFAULT NULL,
  `BOWL` varchar(10) DEFAULT NULL,
  `SET_JARRA_CON_VASO` varchar(10) DEFAULT NULL,
  `OLLA_ARROCERA` varchar(10) DEFAULT NULL,
  `TARRO_PRIMAVERA` varchar(10) DEFAULT NULL,
  `JARRA_1LT` varchar(10) DEFAULT NULL,
  `JARRA_2Lt` varchar(10) DEFAULT NULL,
  `ESCURRIDOR_DE_PLATOS` varchar(10) DEFAULT NULL,
  `CALCULADORA` varchar(10) DEFAULT NULL,
  `RELOJ` varchar(10) DEFAULT NULL,
  `BUTACO` varchar(10) DEFAULT NULL,
  `ESCALERILLA` varchar(10) DEFAULT NULL,
  `VAJILLA` varchar(10) DEFAULT NULL,
  `SET_DE_OLLAS` varchar(10) DEFAULT NULL,
  `TABLET` varchar(10) DEFAULT NULL,
  `BICICLETA` varchar(10) DEFAULT NULL,
  `COLORES` varchar(10) DEFAULT NULL,
  `OBSERVACIONES` text DEFAULT NULL,
  `FECHA_INSTAL` varchar(20) DEFAULT NULL,
  `GESTIONADO` int(1) DEFAULT 0,
  `FECHA_GESTION` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invent`
--

CREATE TABLE `invent` (
  `ID` bigint(10) NOT NULL,
  `CODIGO` bigint(10) DEFAULT NULL,
  `REFERENCIA` varchar(10) DEFAULT NULL,
  `NOMBRE` varchar(51) DEFAULT NULL,
  `DESCRIPCION` varchar(100) DEFAULT NULL,
  `FECHA_VENC` varchar(20) DEFAULT NULL,
  `STOCK_MINIMO` int(3) DEFAULT NULL,
  `STOCK` int(5) DEFAULT NULL,
  `TIPO` varchar(8) DEFAULT NULL,
  `BODEGA` varchar(100) NOT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `PRODUCTO` varchar(70) DEFAULT NULL,
  `ACTIVIDAD` varchar(70) DEFAULT NULL,
  `ESTADO` varchar(100) NOT NULL,
  `TIPO_UBICACION` varchar(20) DEFAULT NULL,
  `ACTIVO` varchar(2) DEFAULT 'SI',
  `VALOR_UNITARIO` varchar(10) DEFAULT NULL,
  `ESTADO_PRODUCTO` varchar(100) DEFAULT NULL,
  `MARCA` varchar(1000) DEFAULT NULL,
  `IMEI` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `invent`
--

INSERT INTO `invent` (`ID`, `CODIGO`, `REFERENCIA`, `NOMBRE`, `DESCRIPCION`, `FECHA_VENC`, `STOCK_MINIMO`, `STOCK`, `TIPO`, `BODEGA`, `CLIENTE`, `PRODUCTO`, `ACTIVIDAD`, `ESTADO`, `TIPO_UBICACION`, `ACTIVO`, `VALOR_UNITARIO`, `ESTADO_PRODUCTO`, `MARCA`, `IMEI`) VALUES
(1, 1, 'honor', 'Celular Honor 8X', 'Prueba', '2023-10-31', 100, 20, '', 'P_Venta 1', 'Honor Honduras', 'Honduras', 'Venta', 'BUEN ESTADO', 'EN BODEGA', 'SI', '8000000', 'Activo', 'honor', '12345'),
(2, 2, 'honor', 'Honor', 'Honor', '2022-01-26', 100, 20, '', 'P_Venta 1', 'Honor Honduras', 'Honduras', 'Venta', 'BUEN ESTADO', 'EN BODEGA', 'SI', '1000', 'Activo', 'honor', 'NO'),
(3, 3, 'Honor', 'Honor 10 lite', 'Honor', '2024-06-11', 100, 200, '', 'P_Venta 1', 'Honor Honduras', 'Honduras', 'Venta', 'BUEN ESTADO', 'EN BODEGA', 'SI', '800000', 'Activo', 'Honor', 'd33444'),
(4, 4, 'Honor', 'Celular honor 10', 'Celular honor 10', '2028-01-20', 100, 700, '', 'P_Venta 1', 'Honor Honduras', 'Honduras', 'Venta', 'BUEN ESTADO', 'EN BODEGA', 'SI', '687500', 'Activo', 'Honor 10', '12548795215451222-77');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invent_11-10-2019`
--

CREATE TABLE `invent_11-10-2019` (
  `ID` bigint(10) NOT NULL,
  `CODIGO` bigint(10) DEFAULT NULL,
  `REFERENCIA` varchar(10) DEFAULT NULL,
  `NOMBRE` varchar(51) DEFAULT NULL,
  `DESCRIPCION` varchar(100) DEFAULT NULL,
  `FECHA_VENC` varchar(20) DEFAULT NULL,
  `STOCK_MINIMO` int(3) DEFAULT NULL,
  `STOCK` int(5) DEFAULT NULL,
  `TIPO` varchar(8) DEFAULT NULL,
  `BODEGA` varchar(100) NOT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `PRODUCTO` varchar(70) DEFAULT NULL,
  `ACTIVIDAD` varchar(70) DEFAULT NULL,
  `ESTADO` varchar(100) NOT NULL,
  `TIPO_UBICACION` varchar(20) DEFAULT NULL,
  `ACTIVO` varchar(2) DEFAULT 'SI',
  `VALOR_UNITARIO` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invent_22-02-2019`
--

CREATE TABLE `invent_22-02-2019` (
  `ID` bigint(10) NOT NULL,
  `CODIGO` bigint(10) DEFAULT NULL,
  `REFERENCIA` varchar(10) DEFAULT NULL,
  `NOMBRE` varchar(51) DEFAULT NULL,
  `DESCRIPCION` varchar(100) DEFAULT NULL,
  `FECHA_VENC` varchar(20) DEFAULT NULL,
  `STOCK_MINIMO` int(3) DEFAULT NULL,
  `STOCK` int(5) DEFAULT NULL,
  `TIPO` varchar(8) DEFAULT NULL,
  `BODEGA` varchar(100) NOT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `PRODUCTO` varchar(70) DEFAULT NULL,
  `ACTIVIDAD` varchar(70) DEFAULT NULL,
  `ESTADO` varchar(100) NOT NULL,
  `TIPO_UBICACION` varchar(20) DEFAULT NULL,
  `ACTIVO` varchar(2) DEFAULT 'SI',
  `VALOR_UNITARIO` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invent_2018-09-20`
--

CREATE TABLE `invent_2018-09-20` (
  `ID` bigint(10) NOT NULL,
  `CODIGO` bigint(10) DEFAULT NULL,
  `REFERENCIA` varchar(10) DEFAULT NULL,
  `NOMBRE` varchar(51) DEFAULT NULL,
  `DESCRIPCION` varchar(100) DEFAULT NULL,
  `FECHA_VENC` varchar(20) DEFAULT NULL,
  `STOCK_MINIMO` int(3) DEFAULT NULL,
  `STOCK` int(5) DEFAULT NULL,
  `TIPO` varchar(8) DEFAULT NULL,
  `BODEGA` varchar(100) NOT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `PRODUCTO` varchar(70) DEFAULT NULL,
  `ACTIVIDAD` varchar(70) DEFAULT NULL,
  `ESTADO` varchar(100) NOT NULL,
  `TIPO_UBICACION` varchar(20) DEFAULT NULL,
  `ACTIVO` varchar(2) DEFAULT 'SI',
  `VALOR_UNITARIO` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invent_ant`
--

CREATE TABLE `invent_ant` (
  `ID` bigint(10) NOT NULL,
  `CODIGO` bigint(10) DEFAULT NULL,
  `REFERENCIA` varchar(10) DEFAULT NULL,
  `NOMBRE` varchar(51) DEFAULT NULL,
  `DESCRIPCION` varchar(100) DEFAULT NULL,
  `FECHA_VENC` varchar(20) DEFAULT NULL,
  `STOCK_MINIMO` int(3) DEFAULT NULL,
  `STOCK` int(5) DEFAULT NULL,
  `TIPO` varchar(8) DEFAULT NULL,
  `BODEGA` varchar(100) NOT NULL,
  `CLIENTE` varchar(50) DEFAULT NULL,
  `PRODUCTO` varchar(70) DEFAULT NULL,
  `ACTIVIDAD` varchar(70) DEFAULT NULL,
  `ESTADO` varchar(100) NOT NULL,
  `TIPO_UBICACION` varchar(20) DEFAULT NULL,
  `ACTIVO` varchar(2) DEFAULT 'SI'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maps`
--

CREATE TABLE `maps` (
  `id` bigint(10) NOT NULL,
  `fecha` varchar(20) DEFAULT NULL,
  `fecha_solicitud` varchar(20) DEFAULT NULL,
  `bodega` varchar(30) DEFAULT NULL,
  `centro_costo` varchar(30) DEFAULT NULL,
  `de` varchar(50) DEFAULT NULL,
  `ruta` varchar(20) DEFAULT NULL,
  `zona` varchar(20) DEFAULT NULL,
  `distr` varchar(20) DEFAULT NULL,
  `codigo_basis` varchar(20) DEFAULT NULL,
  `poblacion` varchar(50) DEFAULT NULL,
  `cliente` varchar(100) DEFAULT NULL,
  `establecimiento` varchar(100) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `devolucion` varchar(2) DEFAULT NULL,
  `tipo_operacion` varchar(20) DEFAULT NULL,
  `fecha_instal` varchar(20) DEFAULT NULL,
  `fecha_retiro` varchar(20) DEFAULT NULL,
  `tel_contacto` varchar(20) DEFAULT NULL,
  `transportador` varchar(50) DEFAULT NULL,
  `observaciones` text DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `instalado` varchar(2) DEFAULT NULL,
  `remi_salida` varchar(20) DEFAULT NULL,
  `estado_instal` varchar(2) DEFAULT NULL,
  `novedad_instal` varchar(20) DEFAULT NULL,
  `estado_recep` varchar(2) DEFAULT NULL,
  `novedad_recep` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsables`
--

CREATE TABLE `responsables` (
  `ID` bigint(10) NOT NULL,
  `NOMBRE_COMPLETO` varchar(100) CHARACTER SET latin1 NOT NULL,
  `CC` bigint(12) NOT NULL,
  `TEL` bigint(15) NOT NULL,
  `EMAIL` varchar(50) CHARACTER SET latin1 NOT NULL,
  `USER` varchar(20) CHARACTER SET latin1 NOT NULL,
  `CONTRASENA` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salidas`
--

CREATE TABLE `salidas` (
  `id` bigint(10) NOT NULL,
  `num_salida` bigint(10) NOT NULL,
  `fecha` varchar(20) CHARACTER SET latin1 NOT NULL,
  `responsable` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `bodega` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `cliente` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `articulo` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `codigo` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `referencia` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `stock` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `canti_salida` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `lugar_entrega` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `devolucion` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `fecha_devolucion` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `destinatario` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `direccion` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `nombre_contacto` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `tel_contacto` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `transportador` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `observaciones` varchar(900) CHARACTER SET latin1 DEFAULT NULL,
  `usuario` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salidas_2018-09-20`
--

CREATE TABLE `salidas_2018-09-20` (
  `id` bigint(10) NOT NULL,
  `num_salida` bigint(10) NOT NULL,
  `fecha` varchar(20) NOT NULL,
  `responsable` varchar(50) DEFAULT NULL,
  `bodega` varchar(40) DEFAULT NULL,
  `cliente` varchar(20) DEFAULT NULL,
  `articulo` varchar(200) DEFAULT NULL,
  `codigo` varchar(10) DEFAULT NULL,
  `referencia` varchar(20) DEFAULT NULL,
  `stock` varchar(10) DEFAULT NULL,
  `canti_salida` varchar(10) DEFAULT NULL,
  `lugar_entrega` varchar(200) DEFAULT NULL,
  `devolucion` varchar(2) DEFAULT NULL,
  `fecha_devolucion` varchar(20) DEFAULT NULL,
  `destinatario` varchar(100) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `nombre_contacto` varchar(50) DEFAULT NULL,
  `tel_contacto` varchar(20) DEFAULT NULL,
  `transportador` varchar(50) DEFAULT NULL,
  `observaciones` varchar(400) DEFAULT NULL,
  `usuario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp`
--

CREATE TABLE `temp` (
  `id` bigint(10) NOT NULL,
  `articulo` varchar(100) CHARACTER SET latin1 NOT NULL,
  `codigo` bigint(10) NOT NULL,
  `referencia` bigint(15) NOT NULL,
  `stock` varchar(20) CHARACTER SET latin1 NOT NULL,
  `cantidad` bigint(10) NOT NULL,
  `usuario` varchar(20) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp2`
--

CREATE TABLE `temp2` (
  `id` bigint(10) NOT NULL,
  `articulo` varchar(100) CHARACTER SET latin1 NOT NULL,
  `codigo` bigint(10) NOT NULL,
  `referencia` bigint(15) NOT NULL,
  `stock` varchar(20) CHARACTER SET latin1 NOT NULL,
  `cantidad` bigint(10) NOT NULL,
  `usuario` varchar(20) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activos_map`
--
ALTER TABLE `activos_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_maps` (`id_maps`);

--
-- Indices de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entradas_11-07-2019`
--
ALTER TABLE `entradas_11-07-2019`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entradas_2018-09-20`
--
ALTER TABLE `entradas_2018-09-20`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entradas_temp`
--
ALTER TABLE `entradas_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial`
--
ALTER TABLE `historial`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `historial-2018-09-20`
--
ALTER TABLE `historial-2018-09-20`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `historial_backup`
--
ALTER TABLE `historial_backup`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `instal_cocacola`
--
ALTER TABLE `instal_cocacola`
  ADD PRIMARY KEY (`NO_REF`);

--
-- Indices de la tabla `instal_cocacola1`
--
ALTER TABLE `instal_cocacola1`
  ADD PRIMARY KEY (`NO_REF`);

--
-- Indices de la tabla `instal_cocacola2`
--
ALTER TABLE `instal_cocacola2`
  ADD PRIMARY KEY (`NO_REF`);

--
-- Indices de la tabla `instal_cocacola_ante`
--
ALTER TABLE `instal_cocacola_ante`
  ADD PRIMARY KEY (`NO_REF`);

--
-- Indices de la tabla `instal_gestion`
--
ALTER TABLE `instal_gestion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `instal_gestion_cocacola`
--
ALTER TABLE `instal_gestion_cocacola`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `invent`
--
ALTER TABLE `invent`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `invent_11-10-2019`
--
ALTER TABLE `invent_11-10-2019`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `invent_22-02-2019`
--
ALTER TABLE `invent_22-02-2019`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `invent_2018-09-20`
--
ALTER TABLE `invent_2018-09-20`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `invent_ant`
--
ALTER TABLE `invent_ant`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `responsables`
--
ALTER TABLE `responsables`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `salidas`
--
ALTER TABLE `salidas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `salidas_2018-09-20`
--
ALTER TABLE `salidas_2018-09-20`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temp2`
--
ALTER TABLE `temp2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activos_map`
--
ALTER TABLE `activos_map`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  MODIFY `id` bigint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `entradas_11-07-2019`
--
ALTER TABLE `entradas_11-07-2019`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `entradas_2018-09-20`
--
ALTER TABLE `entradas_2018-09-20`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `entradas_temp`
--
ALTER TABLE `entradas_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historial`
--
ALTER TABLE `historial`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `historial-2018-09-20`
--
ALTER TABLE `historial-2018-09-20`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historial_backup`
--
ALTER TABLE `historial_backup`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `instal_gestion`
--
ALTER TABLE `instal_gestion`
  MODIFY `ID` bigint(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `instal_gestion_cocacola`
--
ALTER TABLE `instal_gestion_cocacola`
  MODIFY `ID` bigint(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `invent`
--
ALTER TABLE `invent`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `invent_11-10-2019`
--
ALTER TABLE `invent_11-10-2019`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `invent_22-02-2019`
--
ALTER TABLE `invent_22-02-2019`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `invent_2018-09-20`
--
ALTER TABLE `invent_2018-09-20`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `invent_ant`
--
ALTER TABLE `invent_ant`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `maps`
--
ALTER TABLE `maps`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `responsables`
--
ALTER TABLE `responsables`
  MODIFY `ID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `salidas`
--
ALTER TABLE `salidas`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `salidas_2018-09-20`
--
ALTER TABLE `salidas_2018-09-20`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `temp`
--
ALTER TABLE `temp`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `temp2`
--
ALTER TABLE `temp2`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
