<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Documento sin título</title>
  <link rel="stylesheet" type="text/css" href="tcal.css" />
  <script type="text/javascript" src="tcal.js"></script>
  <style type="text/css">
    .botones {
      background-color: #0057b7;
      color: #FFFFFF;
      font-weight: bold;
      font-variant: normal;
    }
  </style>
  <script type="text/javascript">
    function MM_validateForm() {
      if (document.getElementById) {
        var i, p, q, nm, test, num, min, max, errors = '',
          args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
          test = args[i + 2];
          val = document.getElementById(args[i]);
          if (val) {
            nm = val.name;
            if ((val = val.value) != "") {
              if (test.indexOf('isEmail') != -1) {
                p = val.indexOf('@');
                if (p < 1 || p == (val.length - 1)) errors += '- ' + nm + ' must contain an e-mail address.\n';
              } else if (test != 'R') {
                num = parseFloat(val);
                if (isNaN(val)) errors += '- ' + nm + ' must contain a number.\n';
                if (test.indexOf('inRange') != -1) {
                  p = test.indexOf(':');
                  min = test.substring(8, p);
                  max = test.substring(p + 1);
                  if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                }
              }
            } else if (test.charAt(0) == 'R') errors += '- ' + nm + ' is required.\n';
          }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
      }
    }
  </script>
</head>
<body>
  <?php
  $string_intro = getenv("QUERY_STRING");
  parse_str($string_intro);
  $coor_id;
  require('session.php');
  require('conex.php');
  //include './conex.php';
  if (@$_POST['BODEGA'] or @$_POST['DISTR']) {
    $BODEGA = $_POST['DISTR'];
    $CLIENTE = $_POST['CLIENTE'];
    $NOMBRE = $_POST['NOMBRE_DEL_DISPOSITIVO'];
    $DE = $_POST['DE'];
    $RUTA = $_POST['RUTA'];
    $ZONA = $_POST['ZONA'];
    $FECHA_SOLICITUD = $_POST['FECHA_SOLICITUD'];
    $DISTR = $_POST['DISTR'];
    $CODIGO_BASIS = $_POST['CODIGO_BASIS'];
    $NOM_CLIENTE = $_POST['NOM_CLIENTE'];
    $TEL_CLIENTE = $_POST['TEL_CLIENTE'];
    $POBLACION = $_POST['POBLACION'];
    $ESTABLECIMIENTO = $_POST['ESTABLECIMIENTO'];
    $DIRECCION = $_POST['DIRECCION'];
    $DEVOLUCION = $_POST['DEVOLUCION'];
    $OPERACION = $_POST['OPERACION'];
    $FECHA_INS = $_POST['FECHA_INS'];
    $FECHA_RET = $_POST['FECHA_RET'];
    $OBSERVACIONES = $_POST['OBSERVACIONES'];
  } else {
    $BODEGA = '';
    $CLIENTE = '';
    $NOMBRE = '';
    $DE = '';
    $RUTA = '';
    $ZONA = '';
    $FECHA_SOLICITUD = '';
    $DISTR = '';
    $CODIGO_BASIS = '';
    $NOM_CLIENTE = '';
    $TEL_CLIENTE = '';
    $POBLACION = '';
    $ESTABLECIMIENTO = '';
    $DIRECCION = '';
    $DEVOLUCION = '';
    $OPERACION = '';
    $FECHA_INS = '';
    $FECHA_RET = '';
    $OBSERVACIONES = '';
  }
  if ($t_coor == 1) {
    include('include_sup.php');
  }
  if ($t_coor == 2) {
    include('include_sup2-t3.php');
  }
  if ($t_coor == 3) {
    include('include_sup-t3.php');
  }
  if ($t_coor == 5) {
    include('include_sup-t5.php');
  }
  if ($t_coor == 7) {
    include('include_sup-t7.php');
  }
  $Conexion = mysqli_connect($servidor, $usuario, $password) or die("No se Puede conectar al Servidor");
  mysqli_select_db($Conexion, $baseinvent) or die("No se Puede conectar a la base de Datos");
  ?>
  <table width="1200" border="0" align="left">
    <tr>
      <th colspan="3" align="center" valign="middle" class="titulo_formulario" scope="col">INGRESAR MAP<br> </th>
    </tr>
    <tr>
      <td width="1150">
        <form action="form_map.php?coor_id=<?php echo $coor_id; ?>&t_coor=<?php echo $t_coor; ?>&evento=<?php echo $evento; ?>&bodega=<?php echo $bodega; ?>" method="post" name="InsertarMap" id="InsertarCliente" onSubmit="MM_validateForm('NombreC','','R','ApellidoC','','R','fecha_Entrega','','R','fecha_Devolucion','','R','CostoXpagar','','RisNum','Abono','','R','Saldo','','RisNum','CantA','','RisNum');return document.MM_returnValue">
          <br>
          <table width="1140" border="1" cellspacing="5" class="titulo">
            <tr>
              <td align="left" bgcolor="#0057b7" class="texto_formulario">De </td>
              <td colspan="2" align="left">
                <input name="DE" type="text" class="campos_formulario" id="DE" size="40" value="<?php echo $DE; ?>" />
              </td>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Ruta </td>
              <td align="left"><input name="RUTA" type="text" class="campos_formulario" id="RUTA" size="20" value="<?php echo $RUTA; ?>" /></td>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Zona </td>
              <td align="left"><input name="ZONA" type="text" class="campos_formulario" id="ZONA" size="20" value="<?php echo $ZONA; ?>" /></td>
            </tr>
            <tr>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Fecha de solicitud </td>
              <td colspan="2" align="left"><input name="FECHA_SOLICITUD" type="text" class="tcal" id="FECHA_SOLICITUD" value="<?php echo $FECHA_SOLICITUD; ?>" size="10" readonly="readonly" /></td>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Distr </td>
              <td align="left">
                <select name="DISTR" onchange="this.form.submit()" class="fuente" id="DISTR">
                  <option selected><?php echo $DISTR; ?></option>
                  <option>Bogota Norte</option>
                  <option>Bogota Sur</option>
                  <option>Sabana</option>
                  <option>Canales</option>
                  <option>Cali</option>
                  <option>Pereira</option>
                </select>
              </td>
              <td align="left" bgcolor="#0057b7" class="texto_formulario">Codigo Basis </td>
              <td align="left"><input name="CODIGO_BASIS" type="text" class="campos_formulario" id="CODIGO_BASIS" size="20" value="<?php echo $CODIGO_BASIS; ?>" /></td>
            </tr>
            <tr>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Nombre del Cliente </td>
              <td colspan="2" align="left"><input name="NOM_CLIENTE" type="text" class="campos_formulario" id="NOM_CLIENTE" size="40" value="<?php echo $NOM_CLIENTE; ?>" /></td>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Telefono </td>
              <td align="left"><input name="TEL_CLIENTE" type="text" class="campos_formulario" id="TEL_CLIENTE" size="20" value="<?php echo $TEL_CLIENTE; ?>" /></td>
              <td align="left" bgcolor="#0057b7" class="texto_formulario">Poblacion </td>
              <td align="left"><input name="POBLACION" type="text" class="campos_formulario" id="POBLACION" size="20" value="<?php echo $POBLACION; ?>" /></td>
            </tr>
            <tr>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Establecimiento </td>
              <td colspan="2" align="left"><input name="ESTABLECIMIENTO" type="text" class="campos_formulario" id="ESTABLECIMIENTO" size="40" value="<?php echo $ESTABLECIMIENTO; ?>" /></td>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Direccion </td>
              <td colspan="3" align="left"><input name="DIRECCION" type="text" class="campos_formulario" id="DIRECCION" size="50" value="<?php echo $DIRECCION; ?>" /></td>
            </tr>
            <tr>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Devolucion</td>
              <td colspan="2" align="left">
                <select name="DEVOLUCION" onchange="dev(this)" class="fuente" id="DEVOLUCION">
                  <option selected><?php echo $DEVOLUCION; ?></option>
                  <option>SI</option>
                  <option>NO</option>
                </select>
              </td>
            </tr>
            <tr>
            </tr>
            <tr>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Tipo de Operacion</td>
              <td colspan="2" align="left">
                <select name="OPERACION" id="OPERACION">
                  <option selected><?php echo $OPERACION; ?></option>
                  <option>INSTALAR</option>
                  <option>RETIRAR</option>
                  <option>REUBICAR</option>
                  <option>CAMBIAR</option>
                  <option>MANTENIMIENTO</option>
                  <option>COMPRAR</option>
                  <option>ENTREGAR</option>
                </select>
              </td>
              <td bgcolor="#0057b7" align="left" class="texto_formulario">Fecha Instal</td>
              <td align="left"><input name="FECHA_INS" type="text" class="tcal" id="FECHA_INS" value="<?php echo $FECHA_INS; ?>" size="10" readonly="readonly" /></td>
              <td align="left" class="texto_formulario">Fecha Retiro</td>
              <td align="left"><input name="FECHA_RET" type="text" class="tcal" id="FECHA_RET" value="<?php echo $FECHA_RET; ?>" size="10" readonly="readonly" /></td>
            </tr>
            <tr>
              <th align="left" valign="middle" class="texto_formulario">Puntos de ventas</th>
              <th colspan="2" align="left" valign="middle">
                <select name="BODEGA" id="BODEGA" onchange="this.form.submit()">
                  <?PHP
                  echo "<option>" . $BODEGA . "</option>";
                  ?>
                </select>
              </th>
              <th height="26" align="left" valign="middle" class="texto_formulario">CLIENTE</th>
              <th colspan="3" align="left" valign="middle">
                <select name="CLIENTE" id="CLIENTE" onChange="this.form.submit()">
                  <?PHP
                  echo "<option>" . $CLIENTE . "</option>";
                  $Seleccion = mysqli_query($Conexion, "SELECT DISTINCT CLIENTE FROM invent WHERE `BODEGA` = '" . $BODEGA . "' and `CLIENTE` LIKE '%COCA-COLA%' ORDER BY CLIENTE");
                  while ($fila = mysqli_fetch_array($Seleccion)) {
                    echo "<option>" . $fila['CLIENTE'] . "</option>";
                  }
                  ?>
                </select>
              </th>
            </tr>
            <tr>
              <th height="34" align="left" class="texto_formulario">NOMBRE DEL ARTICULO</th>
              <th height="34" colspan="2" align="left" valign="middle">
                <select name="NOMBRE_DEL_DISPOSITIVO" id="NOMBRE_DEL_DISPOSITIVO">
                  <option value="" selected>--Seleccione el articulo--</option>
                  <?PHP
                  $Seleccion = mysqli_query($Conexion, "SELECT * FROM invent WHERE `BODEGA` = '" . $BODEGA . "' AND `CLIENTE` = '" . $CLIENTE . "' ORDER BY NOMBRE");
                  while ($fila = mysqli_fetch_array($Seleccion)) {
                    echo $codigo = $fila['CODIGO'];
                    echo $nombre = $fila['NOMBRE'];
                    echo "<option value=" . $codigo . ">" . $nombre . "</option>";
                  }
                  ?>
                </select>
              </th>
              <th height="34" colspan="2" align="left">
                <input name="aregistro" type="submit" class="botones" formaction="form_map_insert.php?coor_id=<?php echo $coor_id; ?>&t_coor=<?php echo $t_coor; ?>&evento=<?php echo $evento; ?>&bodega=<?php echo $bodega; ?>" onclick="MM_validateForm('CODIGO_BASIS','','R','TEL_CLIENTE','','R','POBLACION','','R','DIRECCION','','R');return document.MM_returnValue" value="Agregar Articulo">
              </th>
            </tr>
            <tr>
              <th height="23" colspan="2" class="texto_formulario">Articulo</th>
              <th height="23" colspan="2" class="texto_formulario">Codigo</th>
              <th height="23" colspan="2" class="texto_formulario">Stock</th>
              <th height="23" colspan="1" class="texto_formulario">CANTIDAD</th>
            </tr>
            <tr>
              <th height="32" colspan="7">
              </th>
            </tr>
            <tr>
              <td height="71" class="texto_formulario">Observaciones:</td>
              <td colspan="6" align="left">
                <textarea name="OBSERVACIONES" cols="90" rows="4" class="campos_formulario" id="NOM_PRODUCTO">
	       <?php
         ?></textarea>
              </td>
            </tr>
            <tr>
              <td colspan="7" align="center">
              </td>
            </tr>
          </table>
          </p>
          <br>
        </form>
      </td>
    </tr>
    <tr>
      <td height="2" colspan="2"></td>
    </tr>
  </table>
  <br />
</body>
</html>