<?php
header("Content-Type: text/html;charset=utf-8");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require $_SERVER['DOCUMENT_ROOT'].'vendor/autoload.php';
?>
<!DOCTYPE html>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<title>Foto Adjunta</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<link href="css/styles.css" rel="stylesheet">
<script type="text/javascript" src="highslide/highslide.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es&libraries=geometry"></script>
<script type="text/javascript" src="js/jquery.js"></script>  
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<style>
.tit
{
	width: 15%;
	font-weight: bold;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
td{
	text-align:left;
}
.volver
{
	padding-top:2%;
	background-image:url(imagenes/atras.png);
	background-repeat:no-repeat;
	width:80%;
	height:40px;	
	color:transparent;
	background-color:transparent;
	border-radius:5px;
	border:1px solid transparent;
}
.boton
{
	padding:3px;
	margin:auto auto;
	width:98.5%;
	height:50px;
	border-radius:8px;
	background-color:#002244;
	color:#FFF;
	font-weight:bold
}
.boton:hover
{
	background-color:#999;
	background: #b4e391; 
	background: -moz-linear-gradient(top, #002244 0%, #00ADE5 50%, #002244 100%); 
	background: -webkit-linear-gradient(top, #002244 0%,#00ADE5 50%,#002244 100%); 
	background: linear-gradient(to bottom, #002244 0%,#00ADE5 50%,#002244 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c45742', endColorstr='#c45742',GradientType=0 ); 
}
.datos {
	font-family:Tahoma, Geneva, sans-serif;	
	font-size: 100%;
	text-transform: uppercase;
	font-size: 12px;
	}
</style>
</head>
<body onLoad="load()">
<?php
 $string_intro = getenv("QUERY_STRING"); 
 parse_str($string_intro);
 $ID = $id;
//if($nreg > 0){
?>
<div>
 <div style="width:100%; text-align:center">
<legend style="color:#0062a7; font-weight:bold;">Evidencia Fotografica Livetrade</legend>
 <table align="center">
  <tr>      
      <td class="botones">
	  <br><br>
      	<?php
		  //session_start();
			require('session.php');
			require('conex.php');

			$Conexion = mysqli_connect($servidor, $usuario, $password) or die("No se Puede conectar al Servidor");

			mysqli_select_db($Conexion, $baseinvent) or die("No se Puede conectar a la base de Datos");

			$product_id = mysqli_query($Conexion, "SELECT `PRODUCT_ID`, `CLIENTE`  FROM `invent` WHERE `invent`.`ID` = $ID ");

			$product = mysqli_fetch_array($product_id);

			if ($product['PRODUCT_ID'])
			{
				$clie = $product['CLIENTE'];

				$cliente = mysqli_query($Conexion, "SELECT *  FROM `cliente` WHERE `cliente`.`nombre` = '$clie'");

				$cliente_info = mysqli_fetch_array($cliente);

				$id_prod = $product['PRODUCT_ID'];

				$exist =  mysqli_query($Conexion, "SELECT * FROM `infor_livetrade_invent` WHERE `infor_livetrade_invent`.`PRODUCT_ID` = $id_prod");

				$exist_id = mysqli_fetch_all($exist);

				if ($exist_id && count($exist_id) > 0)
				{
					foreach ($exist_id as $key => $value) 
					{
						
		?>
						<div> 
							<a href="<?php echo $value[2]; ?>" class="highslide" onClick="return hs.expand(this)"> 
							<img src="<?php echo $value[2]; ?>" alt="" title="Click to enlarge" height="250" width="350" /></a> 
						</div>
		<?php
						$other_infor = json_decode($value[3]);

						if (count($other_infor) > 0)
						{ 
		?>
							<br>
							<h3 style="text-align: center"> Detalles de foto </h3>
		<?php 
							foreach ($other_infor as $key => $act) 
							{ 
								$act = (array) $act;
		?>
								<p><?php echo $act['activity_question']?> : <?php echo $act['activity_answer']?>
							
		<?php		
							}
						}
		?>
						<br><br><br><br>
		<?php
					}
				}
				else
				{
					if (count($cliente_info) > 0)
					{
						if ($cliente_info['cliente_id'] && $cliente_info['activity_id'])
						{
							$url = 'http://livetrade.overall.pe/api/v3';

							$login = json_encode([
								"username" => "livetradeapi",
								"password" => "apiLT-2022"
							]);

							$curl = new \Curl\Curl();
							$curl->setHeader('Content-Type', 'application/json');
							$curl->post($url.'/login', $login);

							if($curl->error)
							{
								var_dump((array) $curl);
							}
							else
							{
								$response = json_decode($curl->response);

								$campana = $cliente_info['campania_id'] ? explode($cliente_info['campania_id']) : [];

								$client = new \GuzzleHttp\Client();
								$PhotogramFeed = $client->request('GET', $url.'/photogram/getFeed', [
									'headers' => [
										'Content-Type' => 'application/json',
										'Accept' => 'application/json',
										'Authorization' => $response->token
									],
									'body' =>  json_encode([
										"start_date" => "2022-02-03",
										"end_date" => "2022-02-03",
										"actividad_id" =>[$cliente_info['activity_id']],
										"campana_id" => $campana,
										"client_id" => [$cliente_info['cliente_id']],
										"rows" => 12,
										"page" => 1
									])
								]);

								$PhotogramFeed = json_decode($PhotogramFeed->getBody()->getContents(), true);

								$id_photogram = [];

								foreach ($PhotogramFeed['data'] as $key => $photo) 
								{
									if ($photo['product_id'] == $id_prod)
									{
										array_push($id_photogram, $photo['id']);
									}
								}

								if (count($id_photogram > 0))
								{
									foreach ($id_photogram as $key => $value) 
									{
										$client2 = new \GuzzleHttp\Client();
										$PhotogramFeedDetail = $client2->request('GET', $url.'/photogram/getFeedPhotoDetail?id='.$value, [
											'headers' => [
												'Content-Type' => 'application/json',
												'Accept' => 'application/json',
												'Authorization' => $response->token
											]
										]);

										$PhotogramFeedDetail = json_decode($PhotogramFeedDetail->getBody()->getContents(), true);
						

		?>

										<div> 
											<a href="<?php echo $PhotogramFeedDetail[0]['answer']; ?>" class="highslide" onClick="return hs.expand(this)"> 
											<img src="<?php echo $PhotogramFeedDetail[0]['answer']; ?>" alt="" title="Click to enlarge" height="250" width="350" /></a> 
										</div>

		<?php

										if (count($PhotogramFeedDetail[0]['other_activities']))
										{ 
		?>
											<br>
											<h3 style="text-align: center"> Detalles de foto </h3>
		<?php 
											foreach ($PhotogramFeedDetail[0]['other_activities'] as $key => $act) 
											{ 
		?>
												<p><?php echo $act['activity_question']?> : <?php echo $act['activity_answer']?>
								
		<?php		
											}
										}
		?>
										<br><br><br><br>
		<?php

										$inform_add = json_encode($PhotogramFeedDetail[0]['other_activities']);
										$url_image = $PhotogramFeedDetail[0]['answer'];

										$Agregar_info = mysqli_query($Conexion, "INSERT INTO `infor_livetrade_invent` (`PRODUCT_ID`, `IMAGE`, `ADDITIONAL_INFORMATION`, `ID_PHOTOGRAM_FEED`) VALUES ('" . $id_prod . "', '" . $url_image . "', '" . $inform_add . "', '" . $value . "');") or die("No se Puede Insertar la información");
									}
								}
								else
								{
		?>
									<legend style="color:#0062a7; font-weight:bold;">El producto no tiene un registro fotografico cargado el LiveTrade</legend>
		<?php
								}
							}
						}
						else
						{
		?>
							<legend style="color:#0062a7; font-weight:bold;">El cliente al que pertenece el producto no tiene configurado la información de Cliente ID o Actividad ID</legend>
		<?php
						}
					}
				}
			}
			else
			{
		?>
				<legend style="color:#0062a7; font-weight:bold;">El producto no tiene un PRODUCT_ID asociado</legend>
		<?php
			}
		?>
	</td>
    </tr>
</table>
      </div>
      <br>
</center>  
</body>    
</html>